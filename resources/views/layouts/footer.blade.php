<!--begin::Footer-->
<div class="footer bg-white py-4 d-flex flex-lg-column " id="kt_footer">
    <!--begin::Container-->
    <div
        class=" container-fluid  d-flex flex-column flex-md-row align-items-center justify-content-between">
        <!--begin::Copyright-->
        <div class="text-dark order-2 order-md-1">
            <span class="text-muted font-weight-bold mr-2">2020&copy;</span>
            <a href="http://notmorales.com" target="_blank"
                class="text-dark-75 text-hover-primary">NotMorales</a>
        </div>
        <!--end::Copyright-->

        <!--begin::Nav-->
        <div class="nav nav-dark">
            <a href="http://keenthemes.com/metronic" target="_blank"
                class="nav-link pl-0 pr-5">Acerca de</a>
            <a href="http://keenthemes.com/metronic" target="_blank" class="nav-link pl-0 pr-5">Equipo</a>
            <a href="http://keenthemes.com/metronic" target="_blank"
                class="nav-link pl-0 pr-0">Contacto</a>
        </div>
        <!--end::Nav-->
    </div>
    <!--end::Container-->
</div>
<!--end::Footer-->