<!DOCTYPE html>
<html lang="es">
<!--begin::Head-->
<head>
    @include('layouts.meta')
</head>
<!--end::Head-->

<!--begin::Body-->
<body id="kt_body"
    class="header-fixed header-mobile-fixed subheader-enabled subheader-fixed subheader-mobile-fixed aside-enabled aside-fixed aside-minimize-hoverable page-loading">


    <div class="d-flex flex-column flex-root">
        <!--begin::Page-->
        <div class="d-flex flex-row flex-column-fluid page">
            @include('layouts.header-mobile')

            @include('layouts.aside')

            <!--begin::Wrapper-->
            <div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">

                @include('layouts.header')   

                <!--begin::Content-->
                <div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
                    @yield('content')
                </div>
                <!--end::Content-->

                @include('layouts.footer')  
                
            </div>

            @include('layouts.config')

        </div>
    </div>

    @include('layouts.user-panel')
    @include('layouts.quick-card')
    @include('layouts.quick-panel')
    @include('layouts.chat-panel')
    @include('layouts.scroll-top')

</body>