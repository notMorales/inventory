@extends('layouts.index')
@section('content')
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6  subheader-solid " id="kt_subheader">
        <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bold my-1 mr-5">
                        Editar Proveedor
                    </h5>
                    <!--end::Page Title-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
            <!--begin::Toolbar-->
            <!--end::Toolbar-->
        </div>
    </div>
    <!--end::Subheader-->

    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class=" container ">
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b example example-compact">
                        <!--begin::Form-->
                        <form method="POST" action="{{ route('proveedor.update', $proveedor) }}" autocomplete="off">
                            @csrf
                            @method('PATCH')

                            @include('proveedor.form')

                        </form>
                        <!--end::Form-->
                        <div class="card-footer">
                            <form method="POST" action="{{ route('proveedor.destroy', $proveedor) }}">
                                @csrf @method('DELETE')
                                <button type="submit" class="btn btn-danger btn-block">¡Eliminar Permanentemente!</button>
                            </form>
                        </div>
                    </div>
                    <!--end::Card-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
@endsection