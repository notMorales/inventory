<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('inicio');

Route::resource('camion', 'CamionController');
Route::resource('categoria', 'CategoriaController');
Route::resource('detalleentrada', 'DetalleEntradaController');
Route::resource('detallesalida', 'DetalleSalidaController');
Route::resource('devolucion', 'DevolucionController');
Route::resource('entrada', 'EntradaController');
Route::resource('inventario', 'InventarioController');
Route::resource('permiso', 'PermisoController');
Route::resource('producto', 'ProductoController');
Route::resource('proveedor', 'ProveedorController');
Route::resource('rol', 'RolController');
Route::resource('rolpermiso', 'RolPermisoController');
Route::resource('salida', 'SalidaController');
Route::resource('unidadmedida', 'UnidadMedidaController');
Route::resource('users', 'UsersController');