-- -----------------------------------------------------
-- Schema inventario
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `inventario` DEFAULT CHARACTER SET utf8 ;
USE `inventario` ;

-- -----------------------------------------------------
-- Table `inventario`.`camion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `inventario`.`camion` (
  `camion_id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) NOT NULL,
  `descripcion` VARCHAR(100) NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` DATETIME NULL,
  PRIMARY KEY (`camion_id`));


-- -----------------------------------------------------
-- Table `inventario`.`categoria`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `inventario`.`categoria` (
  `categoria_id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) NOT NULL,
  `descripcion` VARCHAR(100) NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` DATETIME NULL,
  PRIMARY KEY (`categoria_id`));


-- -----------------------------------------------------
-- Table `inventario`.`proveedor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `inventario`.`proveedor` (
  `proveedor_id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) NOT NULL,
  `correo` VARCHAR(100) NULL,
  `telefono` VARCHAR(100) NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` DATETIME NULL,
  PRIMARY KEY (`proveedor_id`));


-- -----------------------------------------------------
-- Table `inventario`.`unidad_medida`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `inventario`.`unidad_medida` (
  `unidad_medida_id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) NOT NULL,
  `descripcion` VARCHAR(100) NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` DATETIME NULL,
  PRIMARY KEY (`unidad_medida_id`));


-- -----------------------------------------------------
-- Table `inventario`.`producto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `inventario`.`producto` (
  `producto_id` INT NOT NULL AUTO_INCREMENT,
  `categoria_id` INT NOT NULL,
  `proveedor_id` INT NOT NULL,
  `unidad_medida_id` INT NOT NULL,
  `codigo` VARCHAR(25) NOT NULL,
  `nombre` VARCHAR(100) NOT NULL,
  `medida` VARCHAR(100) NULL,
  `descripcion` VARCHAR(100) NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` DATETIME NULL,
  PRIMARY KEY (`producto_id`),
  UNIQUE INDEX `codigo_UNIQUE` (`codigo` ASC),
  INDEX `categoriaFK_producto_idx` (`categoria_id` ASC),
  INDEX `proveedorFK_prodcuto_idx` (`proveedor_id` ASC),
  INDEX `unidadFK_producto_idx` (`unidad_medida_id` ASC),
  CONSTRAINT `categoriaFK_producto`
    FOREIGN KEY (`categoria_id`)
    REFERENCES `inventario`.`categoria` (`categoria_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `proveedorFK_producto`
    FOREIGN KEY (`proveedor_id`)
    REFERENCES `inventario`.`proveedor` (`proveedor_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `unidadFK_producto`
    FOREIGN KEY (`unidad_medida_id`)
    REFERENCES `inventario`.`unidad_medida` (`unidad_medida_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table `inventario`.`inventario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `inventario`.`inventario` (
  `inventario_id` INT NOT NULL AUTO_INCREMENT,
  `producto_id` INT NOT NULL,
  `stock` DOUBLE NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` DATETIME NULL,
  PRIMARY KEY (`inventario_id`),
  INDEX `productoFK_inventario_idx` (`producto_id` ASC),
  CONSTRAINT `productoFK_inventario`
    FOREIGN KEY (`producto_id`)
    REFERENCES `inventario`.`producto` (`producto_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table `inventario`.`salida`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `inventario`.`salida` (
  `salida_id` INT NOT NULL AUTO_INCREMENT,
  `camion_id` INT NOT NULL,
  `folio` INT NOT NULL,
  `fecha` DATETIME NOT NULL,
  `total` DOUBLE NOT NULL,
  `descripcion` VARCHAR(150) NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` DATETIME NULL,
  PRIMARY KEY (`salida_id`),
  INDEX `camionFK_salida_idx` (`camion_id` ASC),
  CONSTRAINT `camionFK_salida`
    FOREIGN KEY (`camion_id`)
    REFERENCES `inventario`.`camion` (`camion_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table `inventario`.`detalle_salida`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `inventario`.`detalle_salida` (
  `detalle_salida_id` INT NOT NULL AUTO_INCREMENT,
  `salida_id` INT NOT NULL,
  `producto_id` INT NOT NULL,
  `unidades` DOUBLE NOT NULL,
  `precio` DOUBLE NOT NULL,
  `total` DOUBLE NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` DATETIME NULL,
  PRIMARY KEY (`detalle_salida_id`),
  INDEX `salidaFK_detalle_salida_idx` (`salida_id` ASC),
  INDEX `productoFK_detalle_salida_idx` (`producto_id` ASC),
  CONSTRAINT `salidaFK_detalle_salida`
    FOREIGN KEY (`salida_id`)
    REFERENCES `inventario`.`salida` (`salida_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `productoFK_detalle_salida`
    FOREIGN KEY (`producto_id`)
    REFERENCES `inventario`.`producto` (`producto_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table `inventario`.`entrada`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `inventario`.`entrada` (
  `entrada_id` INT NOT NULL AUTO_INCREMENT,
  `proveedor_id` INT NOT NULL,
  `factura` VARCHAR(50) NOT NULL,
  `fecha` DATETIME NOT NULL,
  `total` DOUBLE NOT NULL,
  `tipo_pago` VARCHAR(50) NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` DATETIME NULL,
  PRIMARY KEY (`entrada_id`),
  INDEX `proveedorFK_entrada_idx` (`proveedor_id` ASC),
  CONSTRAINT `proveedorFK_entrada`
    FOREIGN KEY (`proveedor_id`)
    REFERENCES `inventario`.`proveedor` (`proveedor_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table `inventario`.`detalle_entrada`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `inventario`.`detalle_entrada` (
  `detalle_entrada_id` INT NOT NULL AUTO_INCREMENT,
  `entrada_id` INT NOT NULL,
  `producto_id` INT NOT NULL,
  `unidades` DOUBLE NOT NULL,
  `precio_unidade` DOUBLE NOT NULL,
  `total` DOUBLE NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` DATETIME NULL,
  PRIMARY KEY (`detalle_entrada_id`),
  INDEX `entradaFK_detalle_entrada_idx` (`entrada_id` ASC),
  INDEX `productoFK_detalle_entrada_idx` (`producto_id` ASC),
  CONSTRAINT `entradaFK_detalle_entrada`
    FOREIGN KEY (`entrada_id`)
    REFERENCES `inventario`.`entrada` (`entrada_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `productoFK_detalle_entrada`
    FOREIGN KEY (`producto_id`)
    REFERENCES `inventario`.`producto` (`producto_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table `inventario`.`devolucion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `inventario`.`devolucion` (
  `devolucion` INT NOT NULL AUTO_INCREMENT,
  `salida_id` INT NOT NULL,
  `producto_id` INT NOT NULL,
  `unidades` DOUBLE NOT NULL,
  `precio` DOUBLE NOT NULL,
  `total` DOUBLE NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` DATETIME NULL,
  PRIMARY KEY (`devolucion`),
  INDEX `salidaFK_devolucion_idx` (`salida_id` ASC),
  INDEX `productoFK_devoluvion_idx` (`producto_id` ASC),
  CONSTRAINT `salidaFK_devolucion`
    FOREIGN KEY (`salida_id`)
    REFERENCES `inventario`.`salida` (`salida_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `productoFK_devoluvion`
    FOREIGN KEY (`producto_id`)
    REFERENCES `inventario`.`producto` (`producto_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table `inventario`.`rol`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `inventario`.`rol` (
  `rol_id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(50) NOT NULL,
  `descripcion` VARCHAR(100) NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` DATETIME NULL,
  PRIMARY KEY (`rol_id`));


-- -----------------------------------------------------
-- Table `inventario`.`Users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `inventario`.`Users` (
  `user_id` INT NOT NULL AUTO_INCREMENT,
  `rol_id` INT NOT NULL,
  `nombre` VARCHAR(100) NOT NULL,
  `telefono` VARCHAR(15) NULL,
  `correo` VARCHAR(100) NULL,
  `direccion` VARCHAR(100) NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` DATETIME NULL,
  PRIMARY KEY (`user_id`),
  INDEX `rolFK_users_idx` (`rol_id` ASC),
  CONSTRAINT `rolFK_users`
    FOREIGN KEY (`rol_id`)
    REFERENCES `inventario`.`rol` (`rol_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table `inventario`.`permiso`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `inventario`.`permiso` (
  `permiso_id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(50) NOT NULL,
  `descripcion` VARCHAR(100) NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` DATETIME NULL,
  PRIMARY KEY (`permiso_id`));


-- -----------------------------------------------------
-- Table `inventario`.`rol_permiso`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `inventario`.`rol_permiso` (
  `rol_permiso_id` INT NOT NULL AUTO_INCREMENT,
  `rol_id` INT NOT NULL,
  `permiso_id` INT NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` DATETIME NULL,
  PRIMARY KEY (`rol_permiso_id`),
  UNIQUE INDEX `unico_rol_permiso` (`rol_id` ASC, `permiso_id` ASC),
  INDEX `permisoFK_rol_permiso_idx` (`permiso_id` ASC),
  CONSTRAINT `rolFK_rol_permiso`
    FOREIGN KEY (`rol_id`)
    REFERENCES `inventario`.`rol` (`rol_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `permisoFK_rol_permiso`
    FOREIGN KEY (`permiso_id`)
    REFERENCES `inventario`.`permiso` (`permiso_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);