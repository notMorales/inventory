<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Categoria extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = ['nombre', 'descripcion', 'created_at', 'updated_at', 'deleted_at'];
    protected $table = 'categoria';
    protected $primaryKey = 'categoria_id';
    
    //Relacion Uno a Mucho
    public function producto(){
        return $this->hasMany('App\Producto', 'categoria_id', 'categoria_id');
    }
}
