<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Salida extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = ['camion_id', 'folio', 'fecha	', 'total', 'descripcion', 'created_at', 'updated_at', 'deleted_at'];
    protected $table = 'salida';
    protected $primaryKey = 'salida_id';
    
    //Relacion Muchos a Uno
    public function camion(){
        return $this->belongsTo('App\Camion', 'camion_id', 'camion_id');
    }

    //Relacion Uno a Mucho
    public function devolucion(){
        return $this->hasMany('App\Devolucion', 'salida_id', 'salida_id');
    }

    //Relacion Uno a Mucho
    public function detalle_salida(){
        return $this->hasMany('App\DetalleSalida', 'salida_id', 'salida_id');
    }
}
