<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UnidadMedida extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = ['nombre', 'descripcion', 'created_at', 'updated_at', 'deleted_at'];
    protected $table = 'unidad_medida';
    protected $primaryKey = 'unidad_medida_id';
    
    //Relacion Uno a Mucho
    public function producto(){
        return $this->hasMany('App\Producto', 'unidad_medida_id', 'unidad_medida_id');
    }
}
