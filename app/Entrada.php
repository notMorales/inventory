<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Entrada extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = ['proveedor_id', 'factura', 'fecha', 'total', 'tipo_pago', 'created_at', 'updated_at', 'deleted_at'];
    protected $table = 'entrada';
    protected $primaryKey = 'entrada_id';
    
    //Relacion Muchos a Uno
    public function proveedor(){
        return $this->belongsTo('App\Proveedor', 'proveedor_id', 'proveedor_id');
    }

    //Relacion Uno a Mucho
    public function detalle_entrada(){
        return $this->hasMany('App\DetalleEntrada', 'entrada_id', 'entrada_id');
    }
}
