<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inventario extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = ['producto_id', 'stock', 'created_at', 'updated_at', 'deleted_at'];
    protected $table = 'inventario';
    protected $primaryKey = 'inventario_id';
    
    //Relacion Uno a Mucho
    public function producto(){
        return $this->belongsTo('App\Producto', 'producto_id', 'producto_id');
    }
}
