<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permiso extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = ['nombre', 'descripcion', 'created_at', 'updated_at', 'deleted_at'];
    protected $table = 'permiso';
    protected $primaryKey = 'permiso_id';
    
    //Relacion Uno a Mucho
    public function rol_permiso(){
        return $this->hasMany('App\RolPermiso', 'permiso_id', 'permiso_id');
    }
}
