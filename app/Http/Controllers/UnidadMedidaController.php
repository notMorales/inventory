<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UnidadMedida;

class UnidadMedidaController extends Controller
{
    
    public function index()
    {
        $unidades = unidadMedida::get();
        return view('unidadMedida.index', [
            'unidades' => $unidades
        ]);
    }

    public function create()
    {
        $unidad = new UnidadMedida();
        return view('unidadmedida.create', [
            'unidad' => $unidad
        ]);
    }

   
    public function store(Request $request)
    {
        request()->validate([
            'nombre' => 'Required | String',
            'descripcion' => ''
        ]);

        UnidadMedida::create([
            'nombre'        => request('nombre'),
            'descripcion'   => request('descripcion'),
        ]);

        return redirect()->route('unidadmedida.index')
            ->with('success', "¡Medida agregada correctamente!" );
    }

    
    public function show($id)
    {
        //
    }

    
    public function edit($unidad)
    {
        return view('unidadmedida.edit', [
            'unidad' => UnidadMedida::find($unidad)
        ]);
    }

    
    public function update(Request $request, $unidad)
    {
        request()->validate([
            'nombre' => 'Required | String',
            'descripcion' => ''
        ]);

        $unidad = UnidadMedida::find($unidad);
        $unidad->nombre = request('nombre');
        $unidad->descripcion = request('descripcion');
        $unidad->save();

        return redirect()->route('unidadmedida.index')
            ->with('success', "¡Medida editada correctamente!" );
    }

   
    public function destroy($unidad)
    {
        try {
            $unidad = UnidadMedida::find($unidad);
            $unidad->delete();
            return redirect()->route('unidadmedida.index')
                ->with('success', "Medida eliminada correctamente.");
        } catch (Throwable $e) {
            return redirect()->route('unidadmedida.index')
            ->with('danger', "No fue posible completar esta operación.");
        }
    }
}
