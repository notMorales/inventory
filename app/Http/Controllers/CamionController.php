<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Camion;

class CamionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $camiones = Camion::get();
        return view('camion.index', [
            'camiones' => $camiones
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $camion = new Camion;
        return view('camion.create', [
            'camion' => $camion
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        request()->validate([
            'nombre' => 'Required | String',
            'descripcion' => ''
        ]);

        Camion::create([
            'nombre'        => request('nombre'),
            'descripcion'   => request('descripcion'),
        ]);

        return redirect()->route('camion.index')
            ->with('success', "¡Camión agregado!" );
    }

    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($camion)
    {
        //
        return view('camion.edit', [
            'camion' => Camion::find($camion)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $camion)
    {
        //
        request()->validate([
            'nombre' => 'Required | String',
            'descripcion' => ''
        ]);

        $camion = Camion::find($camion);
        $camion->nombre = request('nombre');
        $camion->descripcion = request('descripcion');
        $camion->save();

        return redirect()->route('camion.index')
            ->with('success', "¡Camión editado!" );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($camion)
    {
        //
        try {
            $camion = Camion::find($camion);
            $camion->delete();
            return redirect()->route('camion.index')
                ->with('success', "Camión eliminado correctamente.");
        } catch (Throwable $e) {
            return redirect()->route('camion.index')
            ->with('danger', "No fue posible completar esta operación :(.");
        }
    }
}
