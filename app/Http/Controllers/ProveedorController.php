<?php

namespace App\Http\Controllers;

use App\Proveedor;
use Illuminate\Http\Request;

class ProveedorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $proveedores = Proveedor::get();
        return view('proveedor.index', [
            'proveedores' => $proveedores
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $proveedor = new Proveedor;
        return view('proveedor.create', [
            'proveedor' => $proveedor
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'nombre'   => 'Required | String',
            'correo'   => '',
            'telefono' => ''
        ]);

        Proveedor::create([
            'nombre'        => request('nombre'),
            'correo'   => request('correo'),
            'telefono'   => request('telefono'),
        ]);

        return redirect()->route('proveedor.index')
            ->with('success', "¡Proveedor agregado!" );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('proveedor.edit', [
            'proveedor' => Proveedor::find($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate([
            'nombre'   => 'Required | String',
            'correo'   => '',
            'telefono' => ''
        ]);

        $proveedor = Proveedor::find($id);
        $proveedor->nombre   = request('nombre');
        $proveedor->correo   = request('correo');
        $proveedor->telefono = request('telefono');
        $proveedor->save();

        return redirect()->route('proveedor.index')
            ->with('success', "¡Proveedor editado!" );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $proveedor = Proveedor::find($id);
            $proveedor->delete();
            return redirect()->route('proveedor.index')
                ->with('success', "Proveedor eliminado correctamente.");
        } catch (Throwable $e) {
            return redirect()->route('proveedor.index')
            ->with('danger', "No fue posible completar esta operación :(.");
        }
    }
}
