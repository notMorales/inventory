<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;

class CategoriaController extends Controller
{
    public function index()
    {
        $categorias = Categoria::get();
        return view('categoria.index', [
            'categorias' => $categorias
        ]);
    }

    public function create()
    {
        $categoria = new Categoria;
        return view('categoria.create', [
            'categoria' => $categoria
        ]);
    }

    public function store(Request $request)
    {
        request()->validate([
            'nombre' => 'Required | String',
            'descripcion' => ''
        ]);

        Categoria::create([
            'nombre'        => request('nombre'),
            'descripcion'   => request('descripcion'),
        ]);

        return redirect()->route('categoria.index')
            ->with('success', "¡Categoria agregada correctamente!" );
    }

    public function show($id)
    {
        //
    }

    public function edit($categoria)
    {
        return view('categoria.edit', [
            'categoria' => Categoria::find($categoria)
        ]);
    }

    public function update(Request $request, $categoria)
    {
        request()->validate([
            'nombre' => 'Required | String',
            'descripcion' => ''
        ]);

        $categoria = Categoria::find($categoria);
        $categoria->nombre = request('nombre');
        $categoria->descripcion = request('descripcion');
        $categoria->save();

        return redirect()->route('categoria.index')
            ->with('success', "¡Categoria editada correctamente!" );
    }

    public function destroy($categoria)
    {
        try {
            $categoria = Categoria::find($categoria);
            $categoria->delete();
            return redirect()->route('categoria.index')
                ->with('success', "Categoria eliminado correctamente.");
        } catch (Throwable $e) {
            return redirect()->route('categoria.index')
            ->with('danger', "No fue posible completar esta operación.");
        }
    }
}
