<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetalleEntrada extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = ['entrada_id', 'producto_id', 'unidades', 'precio_unidade', 'total', 'created_at', 'updated_at', 'deleted_at'];
    protected $table = 'detalle_entrada';
    protected $primaryKey = 'detalle_entrada_id';
    
    //Relacion Muchos a Uno
    public function producto(){
        return $this->belongsTo('App\Producto', 'producto_id', 'producto_id');
    }

    //Relacion Muchos a Uno
    public function entrada(){
        return $this->belongsTo('App\Entrada', 'entrada_id', 'entrada_id');
    }
}
