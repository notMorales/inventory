<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Camion extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = ['nombre', 'descripcion', 'created_at', 'updated_at', 'deleted_at'];
    protected $table = 'camion';
    protected $primaryKey = 'camion_id';

    //Relacion Uno a Mucho
    public function salida(){
        return $this->hasMany('App\Salida', 'camion_id', 'camion_id');
    }
}
