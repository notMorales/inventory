<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RolPermiso extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = ['rol_id', 'permiso_id', 'created_at', 'updated_at', 'deleted_at'];
    protected $table = 'rol_permiso';
    protected $primaryKey = 'rol_permiso_id';
    
    //Relacion Muchos a Uno
    public function permiso(){
        return $this->belongsTo('App\Permiso', 'permiso_is', 'permiso_is');
    }

    //Relacion Muchos a Uno
    public function rol(){
        return $this->belongsTo('App\Rol', 'rol_id', 'rol_id');
    }
}
