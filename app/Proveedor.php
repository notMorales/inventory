<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Proveedor extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = ['nombre', 'correo', 'telefono', 'created_at', 'updated_at', 'deleted_at'];
    protected $table = 'proveedor';
    protected $primaryKey = 'proveedor_id';
    
    //Relacion Uno a Mucho
    public function producto(){
        return $this->hasMany('App\Producto', 'proveedor_id', 'proveedor_id');
    }

    //Relacion Uno a Mucho
    public function entrada(){
        return $this->hasMany('App\Entrada', 'proveedor_id', 'proveedor_id');
    }
}
