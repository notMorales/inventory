<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rol extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = ['nombre', 'descripcion', 'created_at', 'updated_at', 'deleted_at'];
    protected $table = 'rol';
    protected $primaryKey = 'rol_id';
    
    //Relacion Uno a Mucho
    public function users(){
        return $this->hasMany('App\Users', 'rol_id', 'rol_id');
    }

    //Relacion Uno a Mucho
    public function rol_permiso(){
        return $this->hasMany('App\RolPermiso', 'rol_id', 'rol_id');
    }
}
