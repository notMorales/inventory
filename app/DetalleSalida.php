<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetalleSalida extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = ['salida_id', 'producto_id', 'unidades', 'precio', 'total', 'created_at', 'updated_at', 'deleted_at'];
    protected $table = 'detalle_salida';
    protected $primaryKey = 'detalle_salida_id';
    
    //Relacion Muchos a Uno
    public function producto(){
        return $this->belongsTo('App\Producto', 'producto_id', 'producto_id');
    }

    //Relacion Muchos a Uno
    public function salida(){
        return $this->belongsTo('App\Salida', 'salida_id', 'salida_id');
    }
}
