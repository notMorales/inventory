<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Producto extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = ['categoria_id', 'proveedor_id', 'unidad_medida_id', 'codigo', 'nombre', 'medida', 'descripcion', 'created_at', 'updated_at', 'deleted_at'];
    protected $table = 'producto';
    protected $primaryKey = 'producto_id';
    
    //Relacion Muchos a Uno
    public function categoria(){
        return $this->belongsTo('App\Categoria', 'categoria_id', 'categoria_id');
    }

    //Relacion Muchos a Uno
    public function unidad_medida(){
        return $this->belongsTo('App\UnidadMedida', 'unidad_medida_id', 'unidad_medida_id');
    }

    //Relacion Muchos a Uno
    public function proveedor(){
        return $this->belongsTo('App\Proveedor', 'proveedor_id', 'proveedor_id');
    }

    //Relacion Uno a Uno 
    public function inventario(){
        return $this->hasOne('App\Inventario', 'producto_id', 'producto_id');
    }

    //Relacion Uno a Mucho
    public function devolucion(){
        return $this->hasMany('App\Devolucion', 'producto_id', 'producto_id');
    }

    //Relacion Uno a Mucho
    public function detalle_salida(){
        return $this->hasMany('App\DetalleSalida', 'producto_id', 'producto_id');
    }

    //Relacion Uno a Mucho
    public function detalle_entrada(){
        return $this->hasMany('App\DetalleEntrada', 'producto_id', 'producto_id');
    }
}
