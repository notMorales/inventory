<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Users extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = ['rol_id', 'nombre', 'telefono', 'correo', 'direccion', 'created_at', 'updated_at', 'deleted_at'];
    protected $table = 'users';
    protected $primaryKey = 'user_id';
    
    //Relacion Muchos a Uno
    public function rol(){
        return $this->belongsTo('App\Rol', 'rol_id', 'rol_id');
    }
}
